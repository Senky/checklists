class UsersController < ApplicationController
  before_action :only_owner, only: [:checklists]

  def new
    @user = User.new
  end

  def create
    @user = User.new(user_params)
    if verify_recaptcha(:model => @user, :message => "Oh! It's error with reCAPTCHA!") && @user.save
      log_in @user
      flash[:info] = "User added"
      redirect_to checklists_user_path(@user)
    else
      render 'new'
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "User updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def checklists
    @checklists = Checklist.where(:user_id => params[:id])
  end

  private

    def user_params
      params.require(:user).permit(:username, :email, :password, :password_confirmation)
    end

    def only_owner
      user = User.find(params[:id])
      if !logged_in? or current_user.id != user.id
        flash[:warning] = 'You are not authorized to see this page.'
        redirect_to root_url
      end
    end
end
class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      log_in @user
      claim_ownerships
      #save_checkings
      #cookies[:delete_local_checkings] = true
      redirect_to checklists_user_path(@user)
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_url
  end

  private

    def save_checkings
      return if params[:session][:checkings].empty?

      checkings = JSON.parse(params[:session][:checkings])
      checkings.each do |index, value|
        checking = Checking.new(item_id: index, user_id: @user.id)
        checking.save
      end
    end
end
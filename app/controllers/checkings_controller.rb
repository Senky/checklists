class CheckingsController < ApplicationController
  def create
    item = Item.find(params[:item_id])
    checking = Checking.new(checking_params)
    checking.user_id = current_user.id
    if item.no? && checking.save # automatic items cannot be checked
      render json: {result: true}
    else
      render json: {result: false}
    end
  end

  def destroy()
    Checking.where(:item_id => params[:item_id], :user_id => current_user).first.destroy
    render json: {result: true}
  end

  private

  def checking_params
    params.permit(:item_id)
  end
end

class ChecklistsController < ApplicationController
  before_action :validate_secret_key, only: [:show]
  before_action :validate_private_checklist, only: [:show]
  before_action :validate_ownership, only: [:edit, :update, :destroy]
  before_action :validate_logged_in, only: [:clone]

  def index
    @checklists = Checklist.Public
  end

  def show
    @checklist = Checklist.includes(categories: [:items => [:checkings]]).find(params[:id])
    @markdown = Redcarpet::Markdown.new(Redcarpet::Render::HTML)
  end

  def new
    @checklist = Checklist.new
    @checklist.emails.build
    category = @checklist.categories.build
    item = category.items.build
    item.build_automatic_location_is_http
    item.build_automatic_travis_is_green
    item.build_automatic_coverage_over
  end

  def edit
    @checklist = Checklist.includes(:emails, categories: [items: :automatic_location_is_http]).find(params[:id])

    # build at least one category if it does not exist
    if @checklist.categories.empty?
      category = @checklist.categories.build
      category.items.build
    end

    @checklist.categories.each do |category|
      category.items.each do |item|
        item.build_automatic_location_is_http if item.automatic_location_is_http.nil?
        item.build_automatic_travis_is_green if item.automatic_travis_is_green.nil?
        item.build_automatic_coverage_over if item.automatic_coverage_over.nil?
      end
    end

    # reform emails
    emails = []
    @checklist.emails.each do |e|
      emails << e.email
    end
    @checklist.emails.clear # TODO: find a way not to delete it from DB, just from this collection
    @checklist.emails.build(:email => emails.join("\n"))
  end

  def create
    # remove all items that have no title
    params['checklist']['categories_attributes'].each do |key, category|
      category['items_attributes'].delete_if do |key, item|
        item['title'].strip.empty? ? true : false
      end
    end

    # remove all categories that are left empty
    params['checklist']['categories_attributes'].delete_if do |key, category|
      category['items_attributes'].empty? ? true : false
    end

    # divide emails
    emails = []
    if defined? params['checklist']['emails_attributes']['0']
      params['checklist']['emails_attributes']['0']['email'].split("\n").each do |email|
        emails << {:email => email.strip} if email.strip.length > 0
      end
    end
    params['checklist']['emails_attributes'] = emails

    # filter automatic items
    params['checklist']['categories_attributes'].each do |key, category|
      category['items_attributes'].each do |key, item|
        item.delete :automatic_location_is_http_attributes if item['automatic'] != 'location_is_https'
        item.delete :automatic_travis_is_green_attributes if item['automatic'] != 'travis_is_green'
        item.delete :automatic_coverage_over_attributes if item['automatic'] != 'coverage_over'
      end
    end

    if logged_in?
      @checklist = current_user.checklists.build(checklist_params)
    else
      @checklist = Checklist.new(checklist_params)
    end
    if @checklist.save
      flash[:info] = "Checklist added."
      flash[:info] += ' You can log in to gain ownership of this checklist. You will be able to edit and delete it afterwards' if !logged_in?

      if !logged_in?
        cookies['checklist-' + @checklist.id.to_s] = @checklist.secret_key
      end

      redirect_to checklist_path(:id => @checklist.id, :key => @checklist.secret_key) and return if @checklist.Link?
      redirect_to @checklist
    else
      # recreate at least one category
      if @checklist.categories.empty?
        category = @checklist.categories.build
        category.items.build
      end

      # bind emails together
      @checklist.emails.clear
      @checklist.emails.build(:email => emails.map{|val| val.map{|key, value| value}}.join("\n"))

      render 'new'
    end
  end

  def update
    # remove all items that have no title
    params['checklist']['categories_attributes'].each do |key, category|
      category['items_attributes'].delete_if do |key, item|
        item['title'].strip.empty? ? true : false
      end
    end

    # remove all categories that are left empty
    params['checklist']['categories_attributes'].delete_if do |key, category|
      category['items_attributes'].empty? ? true : false
    end

    # divide emails
    emails = []
    params['checklist']['emails_attributes']['0']['email'].split("\n").each do |email|
      emails << {:email => email.strip} if email.strip.length > 0
    end
    params['checklist']['emails_attributes'] = emails

    # filter automatic items
    params['checklist']['categories_attributes'].each do |key, category|
      category['items_attributes'].each do |key, item|
        item.delete :automatic_location_is_http_attributes if item['automatic'] != 'location_is_https'
        item.delete :automatic_travis_is_green_attributes if item['automatic'] != 'travis_is_green'
        item.delete :automatic_coverage_over_attributes if item['automatic'] != 'coverage_over'
      end
    end

    @checklist = Checklist.find(params[:id])
    if @checklist.update_attributes(checklist_params)
      flash[:info] = "Checklist updated"
      redirect_to checklist_path(:id => @checklist.id, :key => @checklist.secret_key) and return if @checklist.Link?
      redirect_to @checklist
    else
      # recreate at least one category
      if @checklist.categories.empty?
        category = @checklist.categories.build
        category.items.build
      end

      # bind emails together
      @checklist.emails.clear
      @checklist.emails.build(:email => emails.map{|val| val.map{|key, value| value}}.join("\n"))

      render 'new'
    end
  end

  def destroy
    if Checklist.find(params[:id]).destroy
      flash[:info] = "Checklist deleted"
      redirect_to root_path
    end
  end

  def clone
    @checklist = Checklist.find(params[:id])
    new_checklist = @checklist.deep_clone include: {
      :categories => {:items => [
          :automatic_location_is_http,
          :automatic_travis_is_green,
          :automatic_coverage_over
      ]}
    }, validate: false
    if new_checklist.save
      flash[:info] = "Checklist cloned"
      redirect_to checklist_path(:id => new_checklist.id, :key => new_checklist.secret_key) and return if new_checklist.Link?
      redirect_to new_checklist
    else
      flash[:info] = "Unable to clone checklist"
      render 'show'
    end
  end

  def new_secret_key
    require 'securerandom'

    hash = SecureRandom.hex
    checklist = Checklist.find(params[:id]).update(:secret_key => hash)
    flash[:info] = "Secure key updated. See new link below the checklist"
    redirect_to checklist_path(:key => hash)
  end

  private

    def checklist_params
      params.require(:checklist).permit(:title, :sharing,
        categories_attributes: [:id, :title, :_destroy,
          items_attributes: [:id, :title, :link, :description, :optional, :automatic, :_destroy,
            automatic_location_is_http_attributes: [:id, :urls],
            automatic_travis_is_green_attributes: [:id, :repo],
            automatic_coverage_over_attributes: [:id, :repo, :percentage]
          ]
        ],
        emails_attributes: [:email]
      )
    end

    def validate_secret_key
      checklist = Checklist.find(params[:id])
      if checklist.Link? and checklist.secret_key != params[:key]
        flash[:warning] = "Wrong secret key"
        redirect_to root_path
      end
    end

    def validate_private_checklist
      checklist = Checklist.includes(:emails).find(params[:id])
      if checklist.Private?
        if !logged_in? or (checklist.user_id != current_user.id and !checklist.emails.exists?(:email => current_user.email))
          flash[:warning] = "You are not authorized to see this checklist!"
          redirect_to root_path
        end
      end
    end

    def validate_ownership
      checklist = Checklist.find(params[:id])
      if !logged_in? or checklist.user_id != current_user.id
        flash[:warning] = 'You are not authorized to edit this checklist.'
        redirect_to root_path
      end
    end

    def validate_logged_in
      if !logged_in?
        flash[:warning] = 'You must be logged in to do this.'
        redirect_to root_path
      end
    end
end

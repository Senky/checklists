require 'securerandom'

class Checklist < ActiveRecord::Base
  belongs_to :user
  has_many :emails, dependent: :destroy
  has_many :categories, dependent: :destroy
  has_many :items, :through => :categories

  accepts_nested_attributes_for :categories,
                                :allow_destroy => true
  accepts_nested_attributes_for :emails,
                                :allow_destroy => true

  before_validation do 
    errors.add(:cooperation,  "must be unchecked for Public sharing") if Public? and cooperation?
  end
  before_create { self.secret_key = SecureRandom.hex }
  enum sharing: [ :Public, :Private, :Link ]
  validates :title, presence: true, length: { minimum: 3, maximum: 255 }
end

class Item < ActiveRecord::Base
  belongs_to :category
  has_many :checkings, dependent: :destroy
  has_one :automatic_location_is_http, dependent: :destroy
  has_one :automatic_travis_is_green, dependent: :destroy
  has_one :automatic_coverage_over, dependent: :destroy

  enum automatic: [:no, :location_is_https, :travis_is_green, :coverage_over]

  accepts_nested_attributes_for :automatic_location_is_http,
                                :allow_destroy => true
  accepts_nested_attributes_for :automatic_travis_is_green,
                                :allow_destroy => true
  accepts_nested_attributes_for :automatic_coverage_over,
                                :allow_destroy => true
end

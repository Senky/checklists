require 'travis'

class TravisIsGreenWorker
  include Sidekiq::Worker
  def perform()
    repos = AutomaticTravisIsGreen.all

    repos.each do |repo|
      begin
        repo_status = Travis::Repository.find(repo.repo)

        if repo_status.green?
          Checking.new(item_id: repo.item_id).save
        else
          raise 'Repo is red!'
        end
      rescue
        checking = Checking.find_by(item_id: repo.item_id)
        checking.destroy if checking
      end
    end
  end
end
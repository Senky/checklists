require 'open-uri'
require 'open_uri_redirections'

class CoverageOverWorker
  include Sidekiq::Worker
  def perform()
    coverages = AutomaticCoverageOver.all

    coverages.each do |coverage|
      url = ""
      open("https://coveralls.io/repos/#{coverage.repo}/badge.svg?branch=master", :allow_redirections => :safe, :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE) do |resp|
        url = resp.base_uri.to_s
      end

      percentage = url[/[^0-9]+([0-9]+)\.svg/, 1].to_i

      if percentage > coverage.percentage
        Checking.new(item_id: coverage.item_id).save
      else
        checking = Checking.find_by(item_id: coverage.item_id)
        checking.destroy if checking
      end
    end
  end
end
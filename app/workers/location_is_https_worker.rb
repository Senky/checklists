require 'open-uri'
require 'open_uri_redirections'

class LocationIsHttpsWorker
  include Sidekiq::Worker
  def perform()
    item_locations = AutomaticLocationIsHttp.all

    item_locations.each do |locations|
      pass = locations.urls.split("\n").all? do |url|
        open(url, :allow_redirections => :safe, :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE) do |resp|
          url = resp.base_uri.to_s
        end

        url.starts_with? 'https'
      end

      if pass
        Checking.new(item_id: locations.item_id).save
      else
        checking = Checking.find_by(item_id: locations.item_id)
        checking.destroy if checking
      end
    end
  end
end
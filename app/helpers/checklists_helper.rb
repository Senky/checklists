module ChecklistsHelper
  def permission_description(sharing)
    case sharing
      when 'Public'
        return 'Checklist will be visible to everyone. Anyone can clone it and use it. It will be listed in Explore page as well.'
      when 'Private'
        return 'Checklist will be accessible only by users whose email you specify below and you.'
      when 'Link'
        return 'Checklist will be accessible by anyone with specific link. You decide, to with whom you share the link. Also, you can generate new link whenever you want.'
    end
  end
end

module SessionsHelper

  # Logs in the given user.
  def log_in(user)
    session[:user_id] = user.id
    claim_ownerships
  end

  # Returns the current logged-in user (if any).
  def current_user
    @current_user ||= User.find_by(id: session[:user_id])
  end

  # Returns true if the user is logged in, false otherwise.
  def logged_in?
    !current_user.nil?
  end

  # Logs out the current user.
  def log_out
    session.delete(:user_id)
    @current_user = nil
  end

  private

    def claim_ownerships
      claimed = 0

      cookies.select{|key, value| key.start_with?('checklist-')}.each do |key, value|
        id = key.sub('checklist-', '').to_i
        checklist = Checklist.find(id)
        if checklist.secret_key == value
          checklist.update(:user_id => current_user.id)
          claimed += 1
        end
        cookies.delete key
      end

      if claimed > 0
        flash[:info] = "You have claimed ownership of #{claimed > 1 ? claimed + ' checklists' : '1 checklist'}, now listed in Your checklists."
      end
    end
end
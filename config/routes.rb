require 'sidekiq/web'

Rails.application.routes.draw do
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  mount Sidekiq::Web => '/sidekiq'

  root "checklists#new"

  get    'signup'  => 'users#new'
  get    'login'   => 'sessions#new'
  get    'explore'   => 'checklists#index'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
  post   'checkings' => 'checkings#create'
  delete 'checkings' => 'checkings#destroy'

  resources :users do
    member do
      get 'checklists'
    end
  end
  resources :checklists do
    member do
      get 'clone'
      get 'new_secret_key'
    end
  end
  resources :checkings
end

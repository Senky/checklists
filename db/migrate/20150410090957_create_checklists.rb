class CreateChecklists < ActiveRecord::Migration
  def change
    create_table :checklists do |t|
      t.integer :user_id
      t.string :title, null: false
      t.integer :sharing, default: 0
      t.boolean :cooperation, null: false
      t.string :secret_key
      t.timestamps null: false
    end

    add_foreign_key :checklists, :users
  end
end

class AddUniqueKeyToItemIdAndUserIdToCheckings < ActiveRecord::Migration
  def change
    add_index :checkings, [:item_id, :user_id], :unique => true
  end
end

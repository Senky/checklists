class AddDefaultValueToCooperationToChecklists < ActiveRecord::Migration
  def up
    change_column :checklists, :cooperation, :boolean, default: false
  end

  def down
    change_column :checklists, :cooperation, :boolean, null: false
  end
end

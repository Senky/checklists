class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username, unique: true
      t.string :email, unique: true
      t.date :premium_until
      t.string :password_digest
      t.timestamps null: false
    end
  end
end

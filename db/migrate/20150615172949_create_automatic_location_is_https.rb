class CreateAutomaticLocationIsHttps < ActiveRecord::Migration
  def change
    create_table :automatic_location_is_https do |t|
      t.integer :item_id
      t.text :urls

      t.timestamps null: false
    end
  end
end

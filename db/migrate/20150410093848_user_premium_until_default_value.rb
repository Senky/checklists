class UserPremiumUntilDefaultValue < ActiveRecord::Migration
  def up
    change_column :users, :premium_until, :date, default: '2000-01-01 00:00:00'
  end

  def down
    change_column :users, :premium_until, :date, default: nil
  end
end

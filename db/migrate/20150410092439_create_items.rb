class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.integer :category_id
      t.string :title, null: false
      t.string :link
      t.text :description
      t.boolean :optional
      t.integer :automatic, default: 0
    end

    add_foreign_key :items, :categories
  end
end

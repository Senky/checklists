class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.integer :checklist_id
      t.string :email, null: false
      t.timestamps null: false
    end

    add_foreign_key :emails, :checklists
  end
end

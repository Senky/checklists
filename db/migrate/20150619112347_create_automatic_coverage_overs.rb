class CreateAutomaticCoverageOvers < ActiveRecord::Migration
  def change
    create_table :automatic_coverage_overs do |t|
      t.integer :item_id
      t.string :repo
      t.integer :percentage

      t.timestamps null: false
    end
  end
end

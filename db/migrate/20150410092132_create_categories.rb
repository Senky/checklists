class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.integer :checklist_id
      t.string :title, null: false
    end

    add_foreign_key :categories, :checklists
  end
end

class CreateAutomaticTravisIsGreen < ActiveRecord::Migration
  def change
    create_table :automatic_travis_is_greens do |t|
      t.integer :item_id
      t.string :repo

      t.timestamps null: false
    end
  end
end

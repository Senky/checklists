class CreateCheckings < ActiveRecord::Migration
  def change
    create_table :checkings do |t|
      t.integer :user_id
      t.integer :item_id
      t.timestamps null: false
    end

    add_foreign_key :checkings, :users
    add_foreign_key :checkings, :items
  end
end

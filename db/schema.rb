# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150619112347) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "automatic_coverage_overs", force: :cascade do |t|
    t.integer  "item_id"
    t.string   "repo"
    t.integer  "percentage"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "automatic_location_is_https", force: :cascade do |t|
    t.integer  "item_id"
    t.text     "urls"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "automatic_travis_is_greens", force: :cascade do |t|
    t.integer  "item_id"
    t.string   "repo"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "categories", force: :cascade do |t|
    t.integer "checklist_id"
    t.string  "title",        null: false
  end

  create_table "checkings", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "item_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "checkings", ["item_id", "user_id"], name: "index_checkings_on_item_id_and_user_id", unique: true, using: :btree

  create_table "checklists", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "title",                       null: false
    t.integer  "sharing",     default: 0
    t.boolean  "cooperation", default: false, null: false
    t.string   "secret_key"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
  end

  create_table "emails", force: :cascade do |t|
    t.integer  "checklist_id"
    t.string   "email",        null: false
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "items", force: :cascade do |t|
    t.integer "category_id"
    t.string  "title",                   null: false
    t.string  "link"
    t.text    "description"
    t.boolean "optional"
    t.integer "automatic",   default: 0
  end

  create_table "users", force: :cascade do |t|
    t.string   "email"
    t.date     "premium_until",   default: '2000-01-01'
    t.string   "password_digest"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_foreign_key "categories", "checklists"
  add_foreign_key "checkings", "items"
  add_foreign_key "checkings", "users"
  add_foreign_key "checklists", "users"
  add_foreign_key "emails", "checklists"
  add_foreign_key "items", "categories"
end

require_relative '../../app/workers/travis_is_green_worker'

task :travis_is_green do
  TravisIsGreenWorker.perform_async
end
require_relative '../../app/workers/coverage_over_worker'

task :coverage_over do
  CoverageOverWorker.perform_async
end
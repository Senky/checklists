require_relative '../../app/workers/location_is_https_worker'

task :location_is_https do
  LocationIsHttpsWorker.perform_async
end